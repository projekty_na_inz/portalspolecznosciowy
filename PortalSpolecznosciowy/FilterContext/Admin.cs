﻿using System;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.FilterContext
{
    class Admin : ActionFilterAttribute
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string userLogin = filterContext.HttpContext.Session["Login"].ToString();
                Users user = DB.Users.Single(x => x.Login == userLogin);
                if (user.Role != 3)
                {
                    filterContext.Result = new RedirectResult("~/");
                }
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
