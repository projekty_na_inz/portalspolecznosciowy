﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalSpolecznosciowy.FuncHelpers
{
    public class TokenGenerator
    {
        static readonly char[] padding = { '=' };

        public string GenerateToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray()).TrimEnd(padding).Replace('+', '-').Replace('/', '_');;
            return token;
        }

        public bool IsActual(string token, int tokenTime)
        {
            string incoming = token.Replace('_', '/').Replace('-', '+');
            
            byte[] data = Convert.FromBase64String(incoming);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            if (when < DateTime.UtcNow.AddMinutes(tokenTime*(-1)))
                return false;
            else
                return true;
        }
        


    }
}
