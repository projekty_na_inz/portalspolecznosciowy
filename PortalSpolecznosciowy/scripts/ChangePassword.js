﻿$(function() {
    $('#change').click(function (e) {
        var validator = $("form").validate();
        if ($("#Password").val() == null || $("#Password").val() == "" ||
            $("#ConfirmPassword").val() == null || $("#ConfirmPassword").val()== "" ||
            $("#OldPassword").val() == null || $("#OldPassword").val() == "") {
            validator.showErrors({ Password: "Pole nie może być puste" });
        } else {
            $.ajax({
                type: "POST",
                url: "/Login/GetTokenChangePassword",
                data: { login: $("#UserLogin").val() },
                success: function(token) {
                        var oldpasshash = $.sha256($("#OldPassword").val());
                        var con = oldpasshash + token.token;
                        var hash = $.sha256(con);
                        var passhash = $.sha256($("#Password").val());
                        $("#PassHashToken").val(hash);
                        $("#Token").val(token.token);
                        $("#PassHash").val(passhash);
                        $("#Ip").val(token.Ip);
                        $("#Password").val(null);
                        $("#ConfirmPassword").val(null);
                        $("#OldPassword").val(null);
                        $("#changePasswordForm").submit();
                },
                error: function(a, b, c) {
                    console.log(a);
                    console.log(b);
                    console.log(c);
                }
            });
        }
    });
});