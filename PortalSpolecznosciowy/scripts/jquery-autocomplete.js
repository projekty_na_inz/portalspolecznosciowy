﻿$(document).ready(function () {
    $("#tags").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Users/GetUsers",
                type: "POST",
                dataType: "json",
                data: { query: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Login,
                            value: item.Login
                        };
                    }));
                },
                error: function (a, b, c) {
                    console.log(a);
                    console.log(b);
                    console.log(c);
                },
                open: function () {
                    $(this).autocomplete('widget').css('z-index', 1050);
                    return false;
                }
            });
        },
        messages: {
            noResults: "",
            results: function (count) {
                return count + (count > 1 ? ' results' : ' result ') + ' found';
            }
        }
    });
});