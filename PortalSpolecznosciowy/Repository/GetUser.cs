﻿using System.Linq;

namespace PortalSpolecznosciowy.Repository
{
    public class GetUser
    {
        PortalSpolecznosciowyLocalEntities db = new PortalSpolecznosciowyLocalEntities();

        public string UserLogin;
        public int UserID;

        public GetUser(int id)
        {
            UserLogin = db.Users.Single(x => x.ID == id).Login;
        }

        public GetUser(string login)
        {
            UserID = db.Users.Single(x => x.Login == login).ID;
        }

    }
}
