﻿using System.Linq;

namespace PortalSpolecznosciowy.Repository
{
    public class GetRole
    {
        PortalSpolecznosciowyLocalEntities db = new PortalSpolecznosciowyLocalEntities();
        public int Role;

        public GetRole(string login)
        {
            Role = db.Users.Single(x => x.Login == login).Role;
        }

    }
}
