﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalSpolecznosciowy.Repository
{
    public class GetComments
    {
        public IEnumerable<Comments> Comments { get; }

        public GetComments(int artId)
        {
            PortalSpolecznosciowyLocalEntities db = new PortalSpolecznosciowyLocalEntities();
            Comments = db.Comments.Where(x => x.Article_ID == artId).OrderByDescending(x => x.Date).Take(5);
            Comments = Comments.Reverse();
        }



    }
}
