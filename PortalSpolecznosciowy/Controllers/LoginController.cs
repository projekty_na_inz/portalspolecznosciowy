﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.IO;
using PortalSpolecznosciowy.FilterContext;
using PortalSpolecznosciowy.FuncHelpers;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class LoginController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(LoginModel loginModel)
        {
            var token = new TokenGenerator().GenerateToken();
            try
            {
                Users userResponse = DB.Users.Single(x => x.Login == loginModel.Login);
                userResponse.Token = token;
                userResponse.LastLoginData = DateTime.Now;
                userResponse.LastLoginIP = Request.UserHostAddress;
                DB.SaveChanges();
                var ip = Request.UserHostAddress;
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Success = true, token = token, Ip = ip });
            }
            catch (Exception)
            {
                return Json(new { Success = false, token = "error" });
            }
        }

        [HttpPost]
        public ActionResult LoginUserResponse(LoginModel loginModel)
        {
            Users userResponse = DB.Users.Single(x => x.Login == loginModel.Login);
            string hashFromDB = new Sha256().hashSHA256(userResponse.Password + userResponse.Token);

            if (loginModel.PassTokenHash == "error" ||
                Request.UserHostAddress != loginModel.Ip ||
                hashFromDB != loginModel.PassTokenHash ||
                !ModelState.IsValid)
            {
                TempData["Error"] = "Wprowadziłeś nieprawidłowe dane";
                return RedirectToAction("Index");
            }

            if (userResponse.Banned)
            {
                TempData["Error"] = "Twoje konto zostało zbanowane!";
                return RedirectToAction("Index");
            }

            Session["Login"] = userResponse.Login;
            Session["Ip"] = Request.UserHostAddress;

            return RedirectToAction("Index", "MainPanel");
        }

        [SessionExistFilter]
        public ActionResult Logout()
        {
            if (Session["Login"] != null)
            {
                var login = Session["Login"];
                Users userResponse = DB.Users.Single(x => x.Login == login);
                userResponse.Token = new TokenGenerator().GenerateToken();
                DB.SaveChanges();
                Session.Abandon();
                Session.Clear();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult GetTokenChangePassword(string login)
        {
            var token = new TokenGenerator().GenerateToken();
            try
            {
                Users userResponse = DB.Users.Single(x => x.Login == login);
                userResponse.Token = token;
                DB.Users.AddOrUpdate(userResponse);
                DB.SaveChanges();
                var ip = Request.UserHostAddress;
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { Success = true, token = token, Ip = ip });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, token = "error" });
            }
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult ChangePassword(UserEdit ue)
        {
            var userLogin = Session["Login"].ToString();

            if (ue.Ip == Request.UserHostAddress &&
                ue.UserLogin == userLogin)
            {
                var user = DB.Users.Single(x => x.Login == ue.UserLogin);
                Sha256 sha = new Sha256();
                string hashFromDB = sha.hashSHA256(user.Password + user.Token );
                if (user.Token == ue.Token &&
                    new TokenGenerator().IsActual(ue.Token, 1) &&
                    ue.PassHashToken == hashFromDB)
                {
                    user.Password = ue.PasswordHash;
                    DB.SaveChanges();

                    TempData["MessageChangePasswordOption"] = "alert alert-success";
                    TempData["MessageChangePassword"] = "Hasło zostało zmienione!";
                }
                else
                {
                    TempData["MessageChangePasswordOption"] = "alert alert-danger";
                    TempData["MessageChangePassword"] = "Stare hasło jest nieprawidłowe";
                }
            }
            else
            {
                TempData["MessageChangePasswordOption"] = "alert alert-danger";
                TempData["MessageChangePassword"] = "Nieoczekiwany problem!";
            }
            return RedirectToAction("EditProfile", "MainPanel");
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult ChangeProfileImage(UserEdit ue)
        {
            if (ue.NewProfileImage != null && ue.NewProfileImage.ContentLength > 0)
            {
                try
                {
                    Users u = DB.Users.Single(x => x.Login == ue.UserLogin);
                    string extension = Path.GetExtension(ue.NewProfileImage.FileName).ToLower();

                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png")
                    {
                        Image image = Image.FromStream(ue.NewProfileImage.InputStream, true, true);
                        string path = @"~/ProfileImages/" + u.ID + ".jpeg";
                        image.Save(Server.MapPath(path), System.Drawing.Imaging.ImageFormat.Jpeg);
                        u.Profil_image = path;
                        DB.Users.AddOrUpdate(u);
                        DB.SaveChanges();

                        TempData["MessageProfileImageOption"] = "alert alert-success";
                        TempData["MessageProfileImage"] = "Zdjęcie zostało zmienione";
                    }
                    else
                    {
                        throw new Exception("To nie jest obrazek!");
                    }
                }
                catch (Exception ex)
                {
                    TempData["MessageProfileImageOption"] = "alert alert-danger";
                    TempData["MessageProfileImage"] = "Błąd: " + ex.Message;
                }
            }
            else
            {
                TempData["MessageProfileImageOption"] = "alert alert-danger";
                TempData["MessageProfileImage"] = "Musisz wybrać jakieś zdjęcie";
            }
            return RedirectToAction("EditProfile", "MainPanel");
        }

        [HttpPost]
        [SessionExistFilter]
        public ActionResult ChangeEmail(UserEdit ue)
        {
            string userLogin = Session["Login"].ToString();
            if ( !DB.Users.Any(x => x.Email == ue.Email) )
            {
                Users user = DB.Users.Single(x => x.Login == userLogin);
                string server = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                new SendEmail().SendForNewEmail(user, ue.Email, server);
                TempData["MessageChangeEmailOption"] = "alert alert-success";
                TempData["MessageChangeEmail"] = "Na podany adres email zostal wyslany email z linkiem aktywacyjnym. " +
                                                  "Pamiętaj, ze link jest ważny 15 minut!";
            }
            else
            {
                TempData["MessageChangeEmailOption"] = "alert alert-danger";
                TempData["MessageChangeEmail"] = "Adres jest zajety";
            }
            return RedirectToAction("EditProfile", "MainPanel");
        }
        
        public ActionResult ChangeEmailRequest()
        {
            string userLogin = HttpContext.Request.Params.Get("user");
            string token = HttpContext.Request.Params.Get("token");
            string email = HttpContext.Request.Params.Get("email");

            if (new EmailAddressAttribute().IsValid(email) &&
                new TokenGenerator().IsActual(token, 15) &&
                DB.Users.Any(x => x.Login == userLogin) )
            {
                Users user = DB.Users.Single(x => x.Login == userLogin);
                user.Email = email;
                DB.SaveChanges();
                
                TempData["MessageOption"] = "alert alert-success";
                TempData["Message"] = "Adres email został zmieoniony!";
            }
            else
            {
                TempData["MessageOption"] = "alert alert-danger";
                TempData["Message"] = "Link jest nieprawidłowy lub wygasł!. Email nie został zmieniony.";
            }
            
            return View();
        }

    }
}