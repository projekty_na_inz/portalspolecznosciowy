﻿using System;
using System.Linq;
using System.Web.Mvc;
using PortalSpolecznosciowy.FuncHelpers;
using PortalSpolecznosciowy.Models;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Controllers
{
    public class RegisterUserController : Controller
    {
        private PortalSpolecznosciowyLocalEntities DB = new PortalSpolecznosciowyLocalEntities();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegisterUser(RegisterUserView registerUser)
        {
            if (DB.Users.Any(x => x.Login == registerUser.Login))
            {
                TempData["Error"] = "Podany login jest już używany!";
                return RedirectToAction("Index");
            }

            if (DB.Users.Any(x => x.Email == registerUser.Email))
            {
                TempData["Error"] = "Podany email jest już używany!";
                return RedirectToAction("Index");
            }
            
            if (ModelState.IsValid)
            {
                TokenGenerator tg = new TokenGenerator();

                Sha256 sha = new Sha256();
                string hashPasswd = sha.hashSHA256(registerUser.Password);

                Users newUser = new Users
                {
                    Login = registerUser.Login,
                    Password = hashPasswd,
                    Email = registerUser.Email,
                    Role = 1,
                    Active = false,
                    LastLoginData = DateTime.Now,
                    Profil_image = @"~/ProfileImages/DefaultUser.png",
                    Token = tg.GenerateToken(),
                    TokenTime = 15,
                    LastLoginIP = Request.UserHostAddress
                };

                try
                {
                    DB.Users.Add(newUser);
                    DB.SaveChanges();
                    SendEmail sg = new SendEmail();
                    string server = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                    sg.SendForRegister(newUser, server);

                    return View();

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                            //raise a new exception inserting the current one as the InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            else
            {
                return View("Index");
            }

        }

        public ActionResult Activation()
        {
            string userLogin = HttpContext.Request.Params.Get("user");
            string token = HttpContext.Request.Params.Get("token");

            TokenGenerator tg = new TokenGenerator();

            if (userLogin == null || token == null)
            {
                ViewBag.Message = "Podany link jest nieprawidłowy";
            }
            else
            if (DB.Users.Any(u => u.Login == userLogin && u.Token == token) && tg.IsActual(token, 15))
            {
                DB.Users.Single(u => u.Login == userLogin && u.Token == token).Active = true;
                DB.SaveChanges();
                ViewBag.Message = "Konto zostało zaktywowane, teraz możesz się zalogować.";
            }
            else
            {
                ViewBag.Message = "Podany link jest nieprawidłowy lub link aktywacyjny wygasł.";
                ViewBag.param = true;
            }

            return View();
        }

        public ActionResult RepeatActivationLink(string user, string token)
        {
            Users userLogin = DB.Users.Single(u => u.Login == user && u.Token == token);
            TokenGenerator tg = new TokenGenerator();
            userLogin.Token = tg.GenerateToken();
            DB.SaveChanges();

            SendEmail sg = new SendEmail();
            string server = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            sg.SendForRegister(userLogin, server);

            return View();
        }

    }
}