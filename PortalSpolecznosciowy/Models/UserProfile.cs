﻿using System.Collections.Generic;
using System.Web;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Models
{
    public class UserProfile
    {
        public string UserLogin { get; set; }
        public string ProfileImageUrl { get; set; }
        public List<Articles> ArticleList { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CommentContent { set; get; }
        public HttpPostedFileBase File { get; set; }
        //public Comments comments { get; set; }
        public byte FriendStatus { get; set; }
        public int Role { get; set; }

        public IEnumerable<Contacts> UserContacts { get; set; } 
    }
}
