﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace PortalSpolecznosciowy.Models
{
    public class UserEdit
    {
        [Display(Name ="Login")]
        public string UserLogin { get; set; }

        [Display(Name = "Login")]
        public string ProfileImageUrl { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Stare hasło")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nowe hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Potwierdź nowe hasło")]
        [RegularExpression(@"^[a-zA-Z0-9_''-'\s]{8,40}$", ErrorMessage = "Podane znaki są niedozwolone lub hasło jest za krótkie")]
        public string ConfirmPassword { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public HttpPostedFileBase NewProfileImage { get; set; }

        public string PasswordHash { get; set; }

        public string Ip{ get; set; }

        public string Token { get; set; }

        public string PassHashToken { get; set; }
    }
}

