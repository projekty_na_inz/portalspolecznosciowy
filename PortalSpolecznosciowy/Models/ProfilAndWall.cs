﻿using System.Collections.Generic;
using System.Web;
using PortalSpolecznosciowy.Repository;

namespace PortalSpolecznosciowy.Models
{
    public class ProfilAndWall
    {
        public Users user { get; set; }
        public IEnumerable<Articles> articles { get; set; }
        public IEnumerable<Contacts> Contacts { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public Comments comments { get; set; }

        public string ArticleContent { set; get; }
        public string CommentContent { set; get; }
        public HttpPostedFileBase File { get; set; }
    }
}
